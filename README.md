# Viyi.Util - .NET 的常用工具扩展

Viyi.Util v2 对该工具库进行了完全重写，**不**完全兼容 v1。

v2 参考其它现代高级语言、类库和日常使用的情况，提供一些常见的工具函数和工具类，以 C# 的“扩展函数”作为主要实现形式，也包含一些 Helper 静态类和其他工具类。

> ###### 其他相关类库
>
> Viyi.Util 包含内容较多，依赖项也较多，部分功能已经分离出去，按功能分解成一些小工具库
>
> - [Viyi.Strings](https://gitee.com/jamesfancy/viyi-strings) 专注于扩展字符串相关的功能和工具函数
> - Viyi.Bytes (coming soon) 专注于扩展二进制相关的功能和工具函数

## 获取

Viyi.Util 通过 [NuGet](https://www.nuget.org/packages/Viyi.Util/) 发布。

在 [Visual Studio 包管理器控制台](https://docs.microsoft.com/nuget/consume-packages/install-use-packages-powershell) 中运行下面的命令获取：

```
PM> Install-Package Viyi.Util
```

或者在 [管理 NuGet 程序包对话框](https://docs.microsoft.com/nuget/consume-packages/install-use-packages-visual-studio) 中安装。

## 主要特性 (Features)

- 扩展 `object`，

    - 参考 Kotlin，提供了 `.Also()` 和 `.Let()` 扩展，同时支持它们的异步版本；
    - 提供 `ToXxxx()`  和 `ConvertTo()` 方法，是 `System.Convert` 类中相关功能的去异常封装；
    - 提供 `As()` 方法弥补 `as` 关键字不能应用于结构体 (struct) 类型的问题；

- 扩展 Linq (`Enumerable`)，

    - 提供 `ForEach()` / `ForEachAsync` 扩展；

        > 注：Viyi.Util 目前不支持 `IAsyncEnumerable`，请使用 [System.Linq.Async](https://www.nuget.org/packages/System.Linq.Async)。

    - 提供 `AsToArray()` 和 `AsToList()` 先尝试 as 操作，最大避免重新生成数组或列表对象；

    - 为 .NET6 以下框架提供 `MaxBy()` 和 `MinBy()`；

    - 为 `List<T>` 和 `T[]` 分别提供 `Shuffle()` 扩展用来打乱其内部元素的顺序；

    - `ViyiEnumerable<T>.Of(params T[])` 从一组单个对象创建 `IEnumerable<T>`；

- 扩展 `boolean`

    - `it.Then()`，`if / if ... else ...` 的表达式形式；
    - `it.Else()`，`if not` 的表达式形式；
    - `it.Then(fnThen, fnElse)`，` ? : ` 的函数调用形式；
    
- `ViyiRandom` 提供随机数工具

    - `ViyiRandom.Create()` 创建随机数据发生器 (`Random`)

        在 netstandard2.1 和 net6 中使用 `Random.Shared` 来产生随机种子，在 netstandard2.0 中使用类似的方式，通过一个静态且线程安全的 `Random` 对象来产生随机种子。

    - `ViyiRandom.RandomIndexes()` 产生一组以 `0` 开始的随机序数，可用于产生顺序随机的数据

- `EnumHelper` 为枚举类型提供工具方法

    - `EnumHelper.EnumerateWith()` 将枚举中定义的枚举值转换为指定对象的 `IEnumerable`；
    - `EnumHelper.GetDescription()` 获取枚举值的 [`DescriptionAttribute`][description_attribute] 描述信息；
    - `EnumHelper.EnumerateWithDescription()` 与 `EnumerateWith()`类似，但会去分析 [`DescriptionAttribute`][description_attribute] 属性，结果保存在 `EnumEntry` 对象中；
    - `EnumHelper.Enumerate()` 得到 `IEnumerable<KeyValuePair>`；
    - `EnumHelper.ToNameValueCollection()` 得到 `NameValueCollection`（可通过参数交换 Key-Value）

- 对 Compare 和 Equiality 的支持

    - `LambdaCompare` 可以通过 Lambda 来产生 `IComparer<T>` 对象，避免定义类；
    - `LambdaEqualityComparer` 通过 Lambda 产生 `IEqualityComparer<T>` 对象，避免定义类；

- 其它工具

    会根据 [Issue](https://gitee.com/jamesfancy/Viyi.Util/issues) 进行分析、确认并完成实现。

## 目标框架

- [.NET6](https://dotnet.microsoft.com/download/dotnet/6.0)

- [.NET Standard 2.1](https://docs.microsoft.com/dotnet/standard/net-standard?tabs=net-standard-2-1)

    支持：

    - .NET `6.0/5.0`
    - .NET Core `3.1/3.0`

- [.NET Standard 2.0](https://docs.microsoft.com/dotnet/standard/net-standard?tabs=net-standard-2-0)

    支持：

    - .NET `6.0/5.0`
    - .NET Core `3.1/3.0/2.2/2.1/2.0`
    - .NET Framework `4.8/4.7.x/4.7/4.6.2`

## 文档

提供了 Xml Documentation (`Viyi.Util.xml`) 随 NuGet 包发布。

没有制作专门供阅读的 API 文档，但是可以直接阅读源代码中的文档。

[WIKI](https://gitee.com/jamesfancy/Viyi.Util/wikis/) 中对部分功能进行了说明。

## License

[木兰宽松许可证，第2版 (MulanPSL2)](http://license.coscl.org.cn/MulanPSL2/)

> ###### 注：
>
> Viyi.Util `v1.x` 使用 MIT，从 `v2.0` 开始变更为 MulanPSL2



[description_attribute]: https://docs.microsoft.com//dotnet/api/system.componentmodel.descriptionattribute
