namespace Viyi.Util.Build.Builder;

internal interface IBuilder {
    void Build();
    Task BuildAsync();
}

internal abstract class Builder : IBuilder {
    protected TextWriter? Writer { get; set; }
    protected string FilePath { get; }

    protected Builder(string filePath) {
        FilePath = Path.IsPathRooted(filePath)
            ? filePath
            : Path.Combine(this.GetTargetRoot(), filePath);
    }

    public virtual Task BuildAsync() {
        return Task.Run(() => Build());
    }

    public virtual void Build() {
        using var writer = Writer = CreateWriter(FilePath);
        try {
            WriteHeader();
            WriteContent();
            WriteFooter();
        }
        finally {
            Writer = null;
        }
    }

    protected virtual TextWriter CreateWriter(string filePath) {
        return new StreamWriter(filePath);
    }

    protected virtual void Write(string text) {
        Writer?.Write(text);
    }

    protected virtual void WriteIndent(int n) {
        Writer?.Write(string.Concat(Enumerable.Repeat("    ", n)));
    }

    protected virtual void WriteLine(string line) {
        Writer?.WriteLine(line);
    }

    protected virtual void WriteLine() {
        Writer?.WriteLine();
    }

    protected virtual void WriteHeader() {
        WriteLine("// This file is generated by Viyi.Util.Build, please keep it as it is.");
    }

    protected virtual void WriteContent() { }

    protected virtual void WriteFooter() {
        WriteLine("// END");
    }
}

internal static class BuilderExtensions {
    public static string GetTargetRoot(this IBuilder _) {
        var runningFolder = AppDomain.CurrentDomain.BaseDirectory;
        var targetRoot = Path.Combine(
            runningFolder,
            "../../../..",
            "Viyi.Util"
        );
        var root = Path.GetFullPath(targetRoot);
        if (!Directory.Exists(root)) {
            throw new DirectoryNotFoundException($"Not found root directory: {root}");
        }
        return root;
    }
}
