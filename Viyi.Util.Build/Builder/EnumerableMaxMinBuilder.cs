namespace Viyi.Util.Build.Builder;

internal class EnumerableMaxMinBuilder : Builder {
    static readonly string[] Types = new[] {
        "int", "uint", "short", "ushort",
        "long", "ulong", "byte", "sbyte",
        "decimal", "float", "double"
    };

    public EnumerableMaxMinBuilder() : base("Linq/@/Enumerable_MM.gen.cs") {
    }

    protected override void WriteHeader() {
        base.WriteHeader();
        WriteLine(@"
using System;
using System.Collections.Generic;
using System.Linq;

#if NETSTANDARD2_0 || NETSTANDARD2_1
namespace System.Linq;

public static partial class EnumerableExtensions
{");
    }

    protected override void WriteFooter() {
        WriteLine(@"
}
#endif");
        base.WriteFooter();
    }

    protected override void WriteContent() {
        foreach (var type in Types) {
            WriteLine(@$"
    public static T MaxBy<T>(this IEnumerable<T> source, Func<T, {type}> selector) {{
        return source.Aggregate((last, next) =>
            selector(next) > selector(last) ? next : last
        );
    }}

    public static T MinBy<T>(this IEnumerable<T> source, Func<T, {type}> selector) {{
        return source.Aggregate((last, next) =>
            selector(next) < selector(last) ? next : last
        );
    }}");
        }
    }
}
