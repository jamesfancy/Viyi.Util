namespace Viyi.Util.Build.Builder;

internal class ObjectToValuesBuilder : Builder {
    static readonly (string, string)[] Defines = new[] {
        ("byte", "ToByte"),
        ("sbyte", "ToSByte"),
        ("short", "ToInt16"),
        ("ushort", "ToUInt16"),
        ("int", "ToInt32"),
        ("uint", "ToUInt32"),
        ("long", "ToInt64"),
        ("ulong", "ToUInt64"),
        ("decimal", "ToDecimal"),
        ("double", "ToDouble"),
        ("float", "ToSingle"),
        ("bool", "ToBoolean"),
        ("char", "ToChar"),
    };

    public ObjectToValuesBuilder()
        : base("Extensions/Convert/@/ObjectExtensions.ToValue.gen.cs") { }

    protected override void WriteHeader() {
        base.WriteHeader();
        WriteLine(@"
namespace Viyi.Util.Extensions.Convert;

partial class ObjectExtensions {");
    }

    protected override void WriteFooter() {
        WriteLine("}");
        WriteLine();
        base.WriteFooter();
    }

    protected override void WriteContent() {
        foreach (var (typeName, methodName) in Defines) {
            WriteLine(@$"
    /// <summary>
    /// 将object对象转换为{typeName}类型的数据。
    /// </summary>
    /// <param name=""object"">源数据</param>
    /// <param name=""defaultValue"">缺省值</param>
    /// <returns>转换结果数据，如果转换不成功，返回<c>defaultValue</c></returns>
    /// <remarks>与<c>ConvertTo</c>相比，明确指定类型的转换效率更高。</remarks>
    public static {typeName} {methodName}(object @object, {typeName} defaultValue = default) {{
        try {{ return System.Convert.{methodName}(@object); }}
        catch {{ return defaultValue; }}
    }}");
        }
    }
}
