using Viyi.Util.Build.Builder;

const string MutexName = "Viyi.Util.Build$CompileTool";
_ = new Mutex(true, MutexName, out var createNew);
if (!createNew) { return; }

WriteLine("[Viyi Build] Generating codes ... begin");

var builderType = typeof(IBuilder);

var allBuilders = builderType.Assembly
    .GetTypes()
    .Where(type => type.IsClass && !type.IsAbstract)
    .Where(type => type.IsAssignableTo(builderType))
    .Where(type => type.GetConstructor(Array.Empty<Type>()) != null);

foreach (var type in allBuilders) {
    (Activator.CreateInstance(type) as IBuilder)?.Build();
}

WriteLine("[Viyi Build] Generating codes ... done.");
