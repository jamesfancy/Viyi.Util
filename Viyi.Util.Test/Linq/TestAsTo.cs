﻿using System.Collections.Generic;
using System.Linq;
using Viyi.Util.Linq;
using Xunit;

namespace Viyi.Util.Test.Linq; 
public class TestAsTo {
    [Fact]
    public void TestAsToArray() {
        IEnumerable<int> data = Enumerable.Range(0, 10);
        int[] dataArray = data.ToArray();

        var result = data.AsToArray();
        Assert.NotSame(data, result);
        Assert.Equal(data, result);
        Assert.Same(dataArray, dataArray.AsToArray());
    }

    [Fact]
    public void TestAsToList() {
        IEnumerable<int> data = Enumerable.Range(0, 10);
        List<int> dataList = data.ToList();

        var result = data.AsToList();
        Assert.NotSame(data, result);
        Assert.Equal(data, result);
        Assert.Same(dataList, dataList.AsToList());
    }
}
