using Viyi.Util.Linq;
using Xunit;
using Xunit.Abstractions;

namespace Viyi.Util.Test.Linq;
public class TestForEach {
    readonly ITestOutputHelper output;

    public TestForEach(ITestOutputHelper output) {
        this.output = output;
    }

    [Fact]
    public void TestAction() {
        Enumerable.Range(0, 10)
            .Select(n => n * n)
            .ForEach(n => output.WriteLine(n.ToString()));
    }

    [Fact]
    public void TestFunc() {
        Enumerable.Range(10, 10).ForEach(n => n * 10);
    }

    [Fact]
    public void TestException() {
#pragma warning disable CS8600 // Converting null literal or possible null value to non-nullable type.
        Assert.Throws<NullReferenceException>(
            () => {
                IEnumerable<string> source = null;
#pragma warning disable CS8604 // Possible null reference argument.
                source.ForEach(t => t);
#pragma warning restore CS8604 // Possible null reference argument.
            }
        );

        Assert.Throws<NullReferenceException>(
            () => {
                IEnumerable<string> source = new string[1];
#pragma warning disable CS8625 // Cannot convert null literal to non-nullable reference type.
                source.ForEach((Action<string>) null);
#pragma warning restore CS8625 // Cannot convert null literal to non-nullable reference type.
            }
        );
#pragma warning restore CS8600 // Converting null literal or possible null value to non-nullable type.
    }
}
