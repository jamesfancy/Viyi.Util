using Viyi.Util.Linq;
using Xunit;
using Xunit.Abstractions;

namespace Viyi.Util.Test.Utils;

public class TestRandomAndShuffle {
    readonly ITestOutputHelper output;

    public TestRandomAndShuffle(ITestOutputHelper output) {
        this.output = output;
    }

    [Fact]
    public void TestRandomIndexes() {
        var random = ViyiRandom.Create();

        Assert.Equal(Array.Empty<int>(), ViyiRandom.RandomIndexes(0));
        Assert.Equal(new int[] { 0 }, ViyiRandom.RandomIndexes(1));

        Enumerable.Range(0, 10)
            .ForEach(_ => {
                var n = random.Next(2, 30);
                var expect = Enumerable.Range(0, n).ToArray();
                var rIndexes = ViyiRandom.RandomIndexes(n);
                output.WriteLine(string.Join(",", rIndexes));

                Array.Sort(rIndexes);
                Assert.Equal(expect, rIndexes);
            });
    }
}
