namespace Viyi.Util.Collections;

public static class ListExtensions {
    /// <summary>
    /// 对列表内容进行洗牌
    /// </summary>
    /// <remarks>
    /// 注意：该扩展直接对列表元素进行洗牌，不会产生新的列表对象。
    /// </remarks>
    public static List<T> Shuffle<T>(this List<T> source) {
        var random = ViyiRandom.Create();
        for (int i = source.Count - 1; i > 0; i--) {
            var rIndex = random.Next(i + 1);
            (source[i], source[rIndex]) = (source[rIndex], source[i]);
        }
        return source;
    }

    /// <summary>
    /// 对数组内容进行洗牌
    /// </summary>
    /// <remarks>
    /// 注意：该扩展直接对数组元素进行洗牌，不会产生新的数组对象。
    /// </remarks>
    public static T[] Shuffle<T>(this T[] source) {
        var random = ViyiRandom.Create();
        for (int i = source.Length - 1; i > 0; i--) {
            var rIndex = random.Next(i + 1);
            (source[i], source[rIndex]) = (source[rIndex], source[i]);
        }
        return source;
    }
}
