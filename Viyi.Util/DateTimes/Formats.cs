namespace Viyi.Util.DateTimes;

/// <summary>
/// 格式设置接口
/// </summary>
public interface IFormats {
    string? DateTime { get; }
    string? Date { get; }
    string? Time { get; }
}

/// <summary>
/// 日期时间格式参数类
/// </summary>
public class DefinedFormat : IFormats {
    /// <summary>
    /// 默认的，含时间的日期格式，精确到秒
    /// </summary>
    public string? DateTime { get; set; }

    /// <summary>
    /// 日期格式，不含时间
    /// </summary>
    public string? Date { get; set; }

    /// <summary>
    /// 时间格式，不含日期部分，精确到秒
    /// </summary>
    public string? Time { get; set; }
}
