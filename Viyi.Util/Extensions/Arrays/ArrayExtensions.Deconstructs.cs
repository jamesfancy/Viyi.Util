namespace Viyi.Util.Extensions.Arrays;

public static partial class ArrayExtensions {
    public static void Deconstruct<T>(this T[] arr, out T t0, out T t1) {
        CheckArrayLength(arr, 2);
        t0 = arr[0];
        t1 = arr[1];
    }

    public static void Deconstruct<T>(this T[] arr, out T t0, out T t1, out T t2) {
        CheckArrayLength(arr, 3);
        t0 = arr[0];
        t1 = arr[1];
        t2 = arr[2];
    }

    public static void Deconstruct<T>(this T[] arr, out T t0, out T t1, out T t2, out T t3) {
        CheckArrayLength(arr, 4);
        t0 = arr[0];
        t1 = arr[1];
        t2 = arr[2];
        t3 = arr[3];
    }

    public static void Deconstruct<T>(
        this T[] arr,
        out T t0, out T t1, out T t2, out T t3, out T t4
    ) {
        CheckArrayLength(arr, 5);
        t0 = arr[0];
        t1 = arr[1];
        t2 = arr[2];
        t3 = arr[3];
        t4 = arr[4];
    }

    public static void Deconstruct<T>(
        this T[] arr,
        out T t0, out T t1, out T t2, out T t3, out T t4, out T t5
    ) {
        CheckArrayLength(arr, 6);
        t0 = arr[0];
        t1 = arr[1];
        t2 = arr[2];
        t3 = arr[3];
        t4 = arr[4];
        t5 = arr[5];
    }

    static void CheckArrayLength<T>(T[]? arr, int length) {
        if (arr == null || arr.Length < length) {
            throw new ArgumentOutOfRangeException(nameof(arr));
        }
    }
}
