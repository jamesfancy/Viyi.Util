namespace Viyi.Util.Extensions.Arrays;

public static partial class ArrayExtensions {
    public static T[] Pad<T>(this T[]? arr, T value, int length) {
        if (arr?.Length >= length) { return arr; }
        arr ??= Array.Empty<T>();
        return arr.Concat(Enumerable.Repeat(value, length - arr.Length)).ToArray();
    }

    public static T?[] Pad<T>(this T[]? arr, int length) {
        if (arr?.Length >= length) { return arr; }
        arr ??= Array.Empty<T>();
        return arr.Cast<T?>()
            .Concat(Enumerable.Repeat<T?>(default, length - arr.Length))
            .ToArray();
    }

    public static T[] PadLeft<T>(this T[]? arr, T value, int length) {
        if (arr?.Length >= length) { return arr; }
        arr ??= Array.Empty<T>();
        return Enumerable.Repeat(value, length - arr.Length).Concat(arr).ToArray();
    }

    public static T?[] PadLeft<T>(this T[]? arr, int length) {
        if (arr?.Length >= length) { return arr; }
        arr ??= Array.Empty<T>();

        return Enumerable.Repeat<T?>(default, length - arr.Length)
            .Concat(arr.Cast<T?>())
            .ToArray();
    }
}
