namespace Viyi.Util.Extensions;

public static class BooleanExtensions {
    /// <summary>
    /// 对 if ... else 语句的函数式封装，返回调用者自身
    /// </summary>
    public static bool Then(this bool condition, Action fnThen, Action? fnElse = null) {
        if (condition) { fnThen(); }
        else { fnElse?.Invoke(); }
        return condition;
    }

    /// <summary>
    /// 对相反条件的 if 语句进行函数式封装，返回调用者自身
    /// </summary>
    public static bool Else(this bool condition, Action fnElse) {
        if (!condition) { fnElse(); }
        return condition;
    }

    /// <summary>
    /// 指定 then 和 else 表达式，根据 condition 的结果来选择执行并返回
    /// </summary>
    public static T Then<T>(this bool condition, Func<T> fnThen, Func<T> fnElse) {
        return condition ? fnThen() : fnElse();
    }
}
