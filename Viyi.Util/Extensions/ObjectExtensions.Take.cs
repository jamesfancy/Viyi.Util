namespace Viyi.Util.Extensions;

public static partial class ObjectExtensions {
    /// <summary>
    /// 如果给定断言成立，则返回对象/数值，否则返回默认值
    /// </summary>
    public static T? TakeIf<T>(this T me, Func<T, bool> predicate) where T : class {
        return predicate(me) ? me : null;
    }

    /// <summary>
    /// 如果给定条件成立，则返回对象/数值，否则返回默认值
    /// </summary>
    public static T? TakeIf<T>(this T me, bool condition) where T : class {
        return condition ? me : null;
    }

    /// <summary>
    /// 如果给定断言不成立，则返回对象/数值，否则返回默认值
    /// </summary>
    public static T? TakeUnless<T>(this T me, Func<T, bool> predicate) where T : class {
        return predicate(me) ? default : me;
    }

    /// <summary>
    /// 如果给定条件不成立，则返回对象/数值，否则返回默认值
    /// </summary>
    public static T? TakeUnless<T>(this T me, bool condition) where T : class {
        return condition ? default : me;
    }
}
