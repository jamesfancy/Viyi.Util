namespace Viyi.Util;

public sealed class LambdaCompare<T> : IComparer<T> {
    private readonly Func<T?, T?, int> compare;

    public LambdaCompare(Func<T?, T?, int> compare) {
        this.compare = compare;
    }

    public int Compare(T? x, T? y) => compare(x, y);
}
