namespace Viyi.Util;

public sealed class LambdaEqualityComparer<T> : IEqualityComparer<T> {
    private readonly Func<T?, T?, bool> equal;
    private readonly Func<T, int> hash;

    public LambdaEqualityComparer(Func<T?, T?, bool> equal, Func<T, int>? hash = null) {
        this.equal = equal;
        this.hash = hash ?? (t => t?.GetHashCode() ?? 0);
    }

    public bool Equals(T? x, T? y) => equal(x, y);
    public int GetHashCode(T obj) => hash(obj);
}
