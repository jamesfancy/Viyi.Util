namespace Viyi.Util.Linq;

public static partial class EnumerableExtensions {
    public static TSource MaxBy<TSource, TTarget>(
        this IEnumerable<TSource> source,
        Func<TSource, TTarget> selector
    ) {
        var comaprer = Comparer<TTarget>.Default;
        return source.Aggregate((last, next) =>
            comaprer.Compare(selector(next), selector(last)) > 0 ? next : last
        );
    }

    public static TSource MinBy<TSource, TTarget>(
        this IEnumerable<TSource> source,
        Func<TSource, TTarget> selector
    ) {
        var comaprer = Comparer<TTarget>.Default;
        return source.Aggregate((last, next) =>
            comaprer.Compare(selector(next), selector(last)) < 0 ? next : last
        );
    }
}
