namespace Viyi.Util.Linq;

public static class ViyiEnumerable {
    public static IEnumerable<T> Of<T>(params T[] its) {
        return its ?? Enumerable.Empty<T>();
    }
}
