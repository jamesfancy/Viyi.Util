using Viyi.Util.Collections;

namespace Viyi.Util;

public static class ViyiRandom {
#if NET6_0_OR_GREATER
    /// <summary>
    /// 使用 Random.Shared 产生的随机数作为种子来构造一个新的 Random 对象
    /// </summary>
    public static Random Create() {
        return new Random(Random.Shared.Next());
    }
#else
    static readonly Lazy<Random> seedRandom = new(true);

    /// <summary>
    /// 使用一个线程安全的随机数发生器产生的随机数作为种子来构造一个新的 Random 对象
    /// </summary>
    public static Random Create() {
        return new Random(seedRandom.Value.Next());
    }
#endif

    /// <summary>
    /// 产生一个从 0 开始的序号数组，并使用 Fisher-Yates 算法使之乱序
    /// </summary>
    /// <remarks>
    /// 注：如果要对 List&lt;T&gt; 或数组洗牌，可以直接使用它们的 Shuffle 扩展方法
    /// </remarks>
    public static int[] RandomIndexes(int length) {
        return Enumerable.Range(0, length).ToArray().Shuffle();
    }
}
